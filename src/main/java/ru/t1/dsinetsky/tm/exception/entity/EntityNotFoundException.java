package ru.t1.dsinetsky.tm.exception.entity;

import org.jetbrains.annotations.NotNull;

public final class EntityNotFoundException extends GeneralEntityException {

    public EntityNotFoundException(@NotNull final String modelName) {
        super(modelName + " not found!");
    }

}
