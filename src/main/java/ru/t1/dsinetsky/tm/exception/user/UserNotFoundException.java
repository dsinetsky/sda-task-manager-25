package ru.t1.dsinetsky.tm.exception.user;

public final class UserNotFoundException extends GeneralUserException {

    public UserNotFoundException() {
        super("User not found!");
    }

}
