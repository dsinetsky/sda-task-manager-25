package ru.t1.dsinetsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;

public final class VersionDisplayCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = ArgumentConst.CMD_VERSION;

    @NotNull
    public static final String NAME = TerminalConst.CMD_VERSION;

    @NotNull
    public static final String DESCRIPTION = "Shows program version";

    @Override
    public void execute() {
        System.out.println("Version: " + getPropertyService().getApplicationVersion());
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
