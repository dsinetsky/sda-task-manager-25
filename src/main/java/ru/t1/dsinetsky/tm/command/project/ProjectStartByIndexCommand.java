package ru.t1.dsinetsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_PROJECT_START_BY_INDEX;

    @NotNull
    public static final String DESCRIPTION = "Starts project (if any) found by index";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter index of project:");
        final int index = TerminalUtil.nextInt() - 1;
        @NotNull final String userId = getUserId();
        @NotNull final Project project = getProjectService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
        showProject(project);
        System.out.println("Project successfully started!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
