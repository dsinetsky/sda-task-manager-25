package ru.t1.dsinetsky.tm.component;

import javassist.Modifier;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.dsinetsky.tm.api.repository.ICommandRepository;
import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.repository.IUserRepository;
import ru.t1.dsinetsky.tm.api.service.*;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.system.InvalidArgumentException;
import ru.t1.dsinetsky.tm.exception.system.InvalidCommandException;
import ru.t1.dsinetsky.tm.repository.CommandRepository;
import ru.t1.dsinetsky.tm.repository.ProjectRepository;
import ru.t1.dsinetsky.tm.repository.TaskRepository;
import ru.t1.dsinetsky.tm.repository.UserRepository;
import ru.t1.dsinetsky.tm.service.*;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String COMMAND_PACKAGE = "ru.t1.dsinetsky.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    @Getter
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @NotNull
    @Getter
    private final ITestCreateService testCreateService = new TestCreateService(projectService, taskService, userService);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService, propertyService);

    {
        try {
            @NotNull final Reflections reflection = new Reflections(COMMAND_PACKAGE);
            @NotNull final Set<Class<? extends AbstractCommand>> classes = reflection.getSubTypesOf(AbstractCommand.class);
            for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
        } catch (@NotNull final InstantiationException | IllegalAccessException e) {
            loggerService.error(e);
        }
    }

    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) throws InstantiationException, IllegalAccessException {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        registry(clazz.newInstance());
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void terminalRun(@Nullable final String command) throws GeneralException {
        if (command == null) throw new InvalidCommandException();
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void argumentRun(@Nullable final String arg) throws GeneralException {
        if (arg == null) throw new InvalidArgumentException();
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        abstractCommand.execute();
    }

    private boolean argRun(@NotNull final String[] args) throws GeneralException {
        if (args.length < 1) {
            return false;
        }
        @NotNull final String param = args[0];
        argumentRun(param);
        return true;
    }

    private void exitApp() {
        System.exit(0);
    }

    private void initLogger() {
        loggerService.info("Welcome to the task-manager_v." + propertyService.getApplicationVersion() + "!\nType \"help\" for list of commands");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("Thank you for using task-manager!");
            }
        });
    }

    private void initUser() throws GeneralException {
        testCreateService.createTest();
    }

    public void run(@NotNull final String[] args) {
        try {
            if (argRun(args)) {
                exitApp();
                return;
            }
        } catch (@NotNull final GeneralException e) {
            System.err.println(e.getMessage());
            exitApp();
            return;
        }
        initLogger();
        try {
            initUser();
        } catch (@NotNull final GeneralException e) {
            loggerService.error(e);
        }
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nEnter command:");
                @Nullable final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                terminalRun(command);
            } catch (@NotNull final GeneralException e) {
                loggerService.error(e);
            }
        }
    }

}
