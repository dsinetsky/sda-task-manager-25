package ru.t1.dsinetsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.repository.IUserRepository;
import ru.t1.dsinetsky.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @Nullable
    public User add(@NotNull final User user) {
        if (existsById(user.getId())) return null;
        @NotNull final String login = user.getLogin();
        if (login.isEmpty()) return null;
        if (isUserLoginExist(login)) return null;
        @NotNull final String password = user.getPasswordHash();
        if (password.isEmpty()) return null;
        models.add(user);
        return user;
    }

    @Override
    @Nullable
    public User findUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) return null;
        return models
                .stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean isUserLoginExist(@NotNull final String login) {
        return models
                .stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        if (login.isEmpty()) return;
        @Nullable final User user = findUserByLogin(login);
        if (user == null) return;
        models.remove(user);
    }

}
