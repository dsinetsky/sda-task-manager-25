package ru.t1.dsinetsky.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private final static String PROPERTIES_FILE = "application.properties";

    @NotNull
    private final static String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    private final static String DEVELOPER_NAME_KEY = "developer.name";

    @NotNull
    private final static String DEVELOPER_EMAIL_KEY = "developer.email";

    @NotNull
    private final static String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private final static String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private final static String PASSWORD_SECRET_DEFAULT = "456487";

    @NotNull
    private final static String PASSWORD_ITERATION_DEFAULT = "10005";

    @NotNull
    private final static String EMPTY_VALUE = "";

    @NotNull
    private final static Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(PROPERTIES_FILE));
    }

    @NotNull
    private String getEnvFormat(@NotNull final String value) {
        return value.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String keyEnvFormat = getEnvFormat(key);
        if (System.getenv().containsKey(keyEnvFormat)) return System.getenv(keyEnvFormat);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    public int getPasswordIteration() {
        return Integer.parseInt(getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT));
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return getStringValue(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getDeveloperName() {
        return getStringValue(DEVELOPER_NAME_KEY);
    }

    @Override
    @NotNull
    public String getDeveloperEmail() {
        return getStringValue(DEVELOPER_EMAIL_KEY);
    }

}
