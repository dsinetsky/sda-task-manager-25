package ru.t1.dsinetsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.repository.IAbstractRepository;
import ru.t1.dsinetsky.tm.api.service.IAbstractService;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.entity.EntityNotFoundException;
import ru.t1.dsinetsky.tm.exception.field.EntityIdIsEmptyException;
import ru.t1.dsinetsky.tm.exception.field.IndexOutOfSizeException;
import ru.t1.dsinetsky.tm.exception.field.NegativeIndexException;
import ru.t1.dsinetsky.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IAbstractRepository<M>> implements IAbstractService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @NotNull
    protected String getModelType() {
        //first, get repository class name
        final String repositoryClass = repository.getClass().getSimpleName();
        //second, remove last "Repository" charset from class name
        final int indexEnd = repositoryClass.length() - 10;
        //finally, get and return just name of model as string
        return repositoryClass.substring(0, indexEnd);
    }

    @Override
    @NotNull
    public List<M> returnAll() {
        return repository.returnAll();
    }

    @Override
    @NotNull
    public List<M> returnAll(@Nullable final Comparator comparator) {
        if (comparator == null) return returnAll();
        return repository.returnAll(comparator);
    }

    @Override
    @NotNull
    public List<M> returnAll(@Nullable final Sort sort) {
        if (sort == null) return returnAll();
        return repository.returnAll(sort.getComparator());
    }

    @Override
    @Nullable
    public M add(@Nullable final M model) throws GeneralException {
        if (model == null) throw new EntityNotFoundException(getModelType());
        return repository.add(model);
    }

    @Override
    public void clear() throws GeneralException {
        repository.clear();
    }

    @Override
    @NotNull
    public M findById(@Nullable final String id) throws GeneralException {
        if (id == null || id.isEmpty()) throw new EntityIdIsEmptyException(getModelType());
        @Nullable final M model = repository.findById(id);
        if (model == null) throw new EntityNotFoundException(getModelType());
        return model;
    }

    @Override
    @NotNull
    public M findByIndex(final int index) throws GeneralException {
        if (index < 0) throw new NegativeIndexException();
        if (index >= repository.getSize()) throw new IndexOutOfSizeException(repository.getSize());
        return repository.findByIndex(index);
    }

    @Override
    @NotNull
    public M removeById(@Nullable final String id) throws GeneralException {
        if (id == null || id.isEmpty()) throw new EntityIdIsEmptyException(getModelType());
        @Nullable final M model = repository.removeById(id);
        if (model == null) throw new EntityNotFoundException(getModelType());
        return model;
    }

    @Override
    @NotNull
    public M removeByIndex(final int index) throws GeneralException {
        if (index < 0) throw new NegativeIndexException();
        if (index >= repository.getSize()) throw new IndexOutOfSizeException(repository.getSize());
        return repository.removeByIndex(index);
    }

    @Override
    public boolean existsById(@Nullable final String id) throws GeneralException {
        if (id == null || id.isEmpty()) throw new EntityIdIsEmptyException(getModelType());
        return repository.existsById(id);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

}
